#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime_api.h>

/* Device */
__constant__ size_t dRows;
__constant__ size_t dCols;
__constant__ size_t dPitchElements;

/* Host */
cudaError_t cudaError;
size_t rows;
size_t cols;
size_t elementCount;
size_t memSize;
size_t pitch;
size_t pitchElements;
dim3 blockSize;
dim3 blocksCount;
cudaEvent_t start;
cudaEvent_t stop;



/* Errors */
void error(const char *message)
{
	fprintf(stderr, "%s", message);
	exit(1);
}

void checkCudaError(const char *message)
{
	if (cudaError != cudaSuccess) {
		fprintf(stderr, "%s -- %d: %s\n", message, cudaError, cudaGetErrorString(cudaError));
		exit(1);
	}
}

/* Device */
__global__
void init(int *v)
{
    int col = threadIdx.x + blockIdx.x * blockDim.x;
    int row = threadIdx.y + blockIdx.y * blockDim.y;

    if (row >= dRows || col >= dCols)
        return;

    v[row * dPitchElements + col] = row + col;
}

__global__
void transpost(int *dst, const int *src)
{

}

/* Host */
void printGPUProperties(int gpuIndex)
{
    cudaDeviceProp properties;
    cudaError = cudaGetDeviceProperties(&properties, gpuIndex);
    checkCudaError("cudaGetDeviceProperties");

    printf("multiProcessorCount: %d\n", properties.multiProcessorCount);
    printf("maxThreadsPerBlock: %d\n", properties.maxThreadsPerBlock);
    printf("maxThreadsPerMultiProcessor: %d\n", properties.maxThreadsPerMultiProcessor);
}

void do_init(int *v)
{
    cudaEventRecord(start);
    init<<<blocksCount, blockSize>>>(v);
    cudaEventRecord(stop);

    cudaError = cudaEventSynchronize(stop);
    checkCudaError("cudaEventSynchronize do_init");

    float ms;
    cudaError = cudaEventElapsedTime(&ms, start, stop);
    checkCudaError("cudaEventElapsedTime do_init");

    printf("do_init: %fms\n", ms);
}

void do_transpost(int *dst, const int *src)
{
}


/* MAIN */

int main(int argc, char *argv[])
{
    rows = cols = 1024;
    blockSize.x = blockSize.y = 32;

    //Elimina da argv il nome del processo avviato
    argc--;
    argv++;

    //Numero di righe
    if (argc > 0) {
        rows = cols = atoi(argv[0]);
    }

    //Numero di colonne
    if (argc > 1) {
        cols = atoi(argv[1]);
    }

    //Numero di threads.x per blocco
    if (argc > 2) {
        blockSize.x = blockSize.y = atoi(argv[2]);
    }

    //Numero di threads.y per blocco
    if (argc > 3) {
        blockSize.y = atoi(argv[3]);
    }

    //Stampa le caratteristiche della GPU[0]
    printGPUProperties(0);

    //int *hMatrix;
    int *dMatrix;

    //Alloc della matrice (Device) con pitch. Il pitch è la lunghezza della riga in byte.
    cudaError = cudaMallocPitch(&dMatrix, &pitch, rows, cols);
    checkCudaError("cudaMallocPitch dMatrix");
    //Numero di elementi per riga. E' una semplice divisione perchè "pitch" e' un multiplo di 4 (non vale per int3, float3, etc.)
    pitchElements = pitch / sizeof(*dMatrix);

    cudaError = cudaMemcpyToSymbol(dRows, &rows, sizeof(rows), 0, cudaMemcpyHostToDevice);
    checkCudaError("cudaMemcpyToSymbol rows");
    cudaError = cudaMemcpyToSymbol(dCols, &cols, sizeof(cols), 0, cudaMemcpyHostToDevice);
    checkCudaError("cudaMemcpyToSymbol cols");
    cudaError = cudaMemcpyToSymbol(dPitchElements, &pitchElements, sizeof(pitchElements), 0, cudaMemcpyHostToDevice);
    checkCudaError("cudaMemcpyToSymbol pichElements");


    //Elementi matrice (Device)
    elementCount = rows * pitchElements;
    //Dimensione matrice in byte (Device)
    memSize = elementCount * sizeof(*dMatrix);

    /* TODO: Chiedere al professore se è meglio mettere al numeratore gli elementi reali o quelli ricavati dal pitch */
    //Numero di blocchi sull'asse X
    blocksCount.x = (rows + blockSize.x -1) / blockSize.x;
    //Numero di blocchi sull'asse Y
    blocksCount.y = (cols + blockSize.y -1) / blockSize.y;

    //Stampa il numero di thread per il numero di blocchi = numero di elementi
    printf("Threads %dx%d per blocks count %dx%d = %zu\n", blockSize.x, blockSize.y, blocksCount.x, blocksCount.y, elementCount);

    //Imposta tutti i valori della matrice (Device) a "-1"
    cudaError = cudaMemset2D(dMatrix, pitch, -1, rows, cols);
    checkCudaError("cudaMemset2D dMatrix");

    //Inizializza i cudaEvent_t
    cudaError = cudaEventCreate(&start);
    checkCudaError("cudaEventCreate start");
    cudaError = cudaEventCreate(&stop);
    checkCudaError("cudaEventCreate stop");

    //Inizializza i valori della matrice
    do_init(dMatrix);

    cudaFree(dMatrix);

	return 0;
}
