//
//  main.c
//  OpenCL
//
//  Created by Alberto Ottimo on 21/12/14.
//
//

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "CLPrint.h"

#define BUFFER_SIZE 2048
#define N 4

#define PUTCHAR(buf, index, val) (buf)[(index)>>2] = ((buf)[(index)>>2] & ~(0xffU << (((index) & 3) << 3))) + ((val) << (((index) & 3) << 3))
#define GETCHAR(buf, index) (((unsigned char*)(buf))[(index)])

unsigned char charset[36] = "abcdefghijklmnopqrstuvwxyz0123456789";

void addStringToKeys(char const * str, uint index, uint * keys)
{
    int i;
    for (i = 0; i < 4; i++) {
        keys[index * N + i] = 0x0000;
    }
    
    for (i = 0; i < strlen(str); i++) {
        PUTCHAR(&keys[index * N], i, str[i]);
    }
    
//    unsigned char * keysUChar = (unsigned char *)keys;
//    
//    for (int i = 0; i < strlen(str); i++) {
//        keysUChar[index * N + i] = (unsigned char)str[i];
//    }
//memset(&keys[index], *str, strlen(str));
}

void printHash(uint * hash)
{
    for (int i = 0; i < 16; i++) {
        if (i != 0 && i % 4 == 0)
            printf(" ");
        
        printf("%02x", GETCHAR(hash, i));
        
    }
    printf("\n");
}

int main(int argc, const char * argv[]) {
    
    int platformIndex = -1;
    int deviceIndex = -1;
    char buffer[BUFFER_SIZE];
    
    cl_platform_id platform = NULL;
    cl_device_id device;
    
    //Parameter #1: platform
    if (argc > 1) platformIndex = atoi(argv[1]);
    //Parameter #2: device
    if (argc > 2) deviceIndex = atoi(argv[2]);
    
    //If user not select platform
    if (argc < 2) {
        
        do {
            CLPrintPlatforms();
            
            printf("Select Platform: ");
            fgets(buffer, BUFFER_SIZE, stdin);
            
            platformIndex = atoi(buffer);
            printf("\n");
        } while ((platform = CLSelectPlatform(platformIndex)) == NULL);
    }
    
    //If user not select device
    if (argc < 3) {
        
        do {
            CLPrintDevices(platform);
            printf("Select Device: ");
            fgets(buffer, BUFFER_SIZE, stdin);
            
            deviceIndex = atoi(buffer);
            printf("\n");
        } while ((device = CLSelectDevice(platform, deviceIndex)) == NULL);
    }
    
    cl_context context = CLCreateContext(platform, device);
    cl_command_queue queue = CLCreateQueue(context, device);
    cl_program program = CLCreateProgram(context, device, "Kernels.ocl");

    cl_event event;
    
    uint numberOfWords = 256 * 1024;
    uint dataSize = sizeof(uint) * N * numberOfWords;
    uint * words = malloc(dataSize);
//    addStringToKeys("ciao", 0, words);
//    addStringToKeys("oaic", 1, words);
//    addStringToKeys("oaic2", 2, words);
    
    
//    char * str = malloc(sizeof(uint) * N);
//    for (int i = 0; i < numberOfWords / 36 + 1; i++) {
//        for (int j = 0; j < 36 && (i*36 + j) < numberOfWords ; j++) {
//            str[i] = charset[j];
//            addStringToKeys(str, j, words);
//            //printf("%s\n", str);
//        }
//        str[i] = charset[i%36];
//    }


    uint dataInfo[2] = {N * sizeof(uint), numberOfWords};
    
    cl_mem dataInfo_d = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(dataInfo), NULL, NULL);
    cl_mem words_d = clCreateBuffer(context, CL_MEM_READ_WRITE, dataSize, NULL, NULL);
    cl_mem hashes_d = clCreateBuffer(context, CL_MEM_READ_WRITE, dataSize, NULL, NULL);
    
    clEnqueueWriteBuffer(queue, dataInfo_d, CL_TRUE, 0, sizeof(dataInfo), dataInfo, 0, NULL, NULL);
    clEnqueueWriteBuffer(queue, words_d, CL_TRUE, 0, dataSize, words, 0, NULL, NULL);
    
    //Kernel
    cl_kernel md5Kernel = CLCreateKernel(program, "md5");
    clSetKernelArg(md5Kernel, 0, sizeof(cl_mem), &dataInfo_d);
    clSetKernelArg(md5Kernel, 1, sizeof(cl_mem), &words_d);
    clSetKernelArg(md5Kernel, 2, sizeof(cl_mem), &hashes_d);
    
    
    size_t gws = numberOfWords;
    
    clEnqueueNDRangeKernel(queue, md5Kernel, 1, NULL, &gws, NULL, 0, NULL, &event);
    clFinish(queue);
    
    uint * hashes = malloc(dataSize);
    clEnqueueReadBuffer(queue, hashes_d, CL_TRUE, 0, dataSize, hashes, 1, &event, NULL);
    
//    for (int i = 0; i < numberOfWords; i++) {
//        printHash(&hashes[i * N]);
//    }
//    printf("\n");
    cl_ulong time_start, time_end;
    double total_time;
    
    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
    
    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
    
    total_time = time_end-time_start;
    
    printf("OpenCl Execution time is: %0.3f \n",total_time/1000000.0);
    
    return EXIT_SUCCESS;
}


