//
//  TerminalPrint.h
//  OpenCL
//
//  Created by Alberto Ottimo on 21/12/14.
//
//

#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>

void printCenterString(const char *string, const char mark);
void printEndLine(const char mark);
