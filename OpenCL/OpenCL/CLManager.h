//
//  CLManager.h
//  OpenCL
//
//  Created by Alberto Ottimo on 22/12/14.
//
//

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif


void CLErrorCheck(cl_int error, const char * function, const char * message);
cl_platform_id CLSelectPlatform(int platformIndex);
cl_device_id CLSelectDevice(cl_platform_id platform, int deviceIndex);
cl_context CLCreateContext(cl_platform_id platform, cl_device_id device);
cl_command_queue CLCreateQueue(cl_context context, cl_device_id device);
cl_program CLCreateProgram(cl_context context, cl_device_id device, const char * fileName);
cl_kernel CLCreateKernel(cl_program program, const char * kernelName);
