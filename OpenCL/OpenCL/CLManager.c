//
//  CLManager.c
//  OpenCL
//
//  Created by Alberto Ottimo on 22/12/14.
//
//

#include "CLManager.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "TerminalPrint.h"
#define BUFFER_SIZE (8*1024)

void CLErrorCheck(cl_int error, const char * function, const char * message)
{
    if (error != CL_SUCCESS) {
        fprintf(stderr, "%s - %s: %d", function, message, error);
        exit(EXIT_FAILURE);
    }
}

cl_platform_id CLSelectPlatform(int platformIndex)
{
    if (platformIndex >= 0) {
        
        cl_int err;
        cl_uint nPlatforms;
        cl_platform_id *platforms;
        
        err = clGetPlatformIDs(0, NULL, &nPlatforms);
        CLErrorCheck(err, "clGetPlatformIDs", "get nPlatforms");
        
        platforms = (cl_platform_id *)calloc(nPlatforms, sizeof(cl_platform_id));
        
        err = clGetPlatformIDs(nPlatforms, platforms, NULL);
        CLErrorCheck(err, "clGetPlatformIDs", "get platforms IDs");
        
        if (platformIndex < nPlatforms) {
            return platforms[platformIndex];
        }
    }
    return NULL;
}

cl_device_id CLSelectDevice(cl_platform_id platform, int deviceIndex)
{
    if (platform && deviceIndex >= 0) {
    
        cl_int err;
        cl_uint nDevices;
        cl_device_id *devices;
        
        err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &nDevices);
        CLErrorCheck(err, "clGetDeviceIDs", "get nDevices");
        
        devices = (cl_device_id *)calloc(nDevices, sizeof(cl_device_id));
        
        err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, nDevices, devices, NULL);
        CLErrorCheck(err, "clGetDeviceIDs", "get device IDs");
        
        
        if (deviceIndex < nDevices) {
            return devices[deviceIndex];
        }
    }
    return NULL;
}

cl_context CLCreateContext(cl_platform_id platform, cl_device_id device)
{
    cl_int err;
    cl_context_properties contextProperties[] = {CL_CONTEXT_PLATFORM, (cl_context_properties)platform, 0};
    cl_context context = clCreateContext(contextProperties, 1, &device, NULL, NULL, &err);
    CLErrorCheck(err, "clCreateContext", "create context");
    return context;
}

cl_command_queue CLCreateQueue(cl_context context, cl_device_id device)
{
    cl_int err;
    cl_command_queue queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
    CLErrorCheck(err, "clCreateCommandQueue", "create queue");
    return queue;
}

cl_program CLCreateProgram(cl_context context, cl_device_id device, const char * fileName)
{
    cl_int err;
    cl_program program;
    
    char *buffer = (char *)calloc(BUFFER_SIZE, sizeof(char));
    time_t now = time(NULL);
    snprintf(buffer, BUFFER_SIZE-1, "//%s#include \"%s\"\n", ctime(&now), fileName);
    
    printf("'codice':\n%s\n", buffer);
    const char *ptrBuff = buffer;
    program = clCreateProgramWithSource(context, 1, &ptrBuff, NULL, &err);
    CLErrorCheck(err, "clCreateProgramWithSource", "create program");
    
    cl_int build_err;
    
    build_err = clBuildProgram(program, 1, &device, "-I.", NULL, NULL);
    
    err = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, BUFFER_SIZE, buffer, NULL);
    CLErrorCheck(err, "clGetProgramBuildInfo", "get program build info");

    
    printCenterString("BUILD LOG", '=');
    printf("%s", buffer);
    printEndLine('=');
    
    CLErrorCheck(build_err, "clBuildProgram", "build program");

    
    return program;
}

cl_kernel CLCreateKernel(cl_program program, const char * kernelName)
{
    cl_int err;
    cl_kernel kernel;
    
    kernel = clCreateKernel(program, kernelName, &err);
    CLErrorCheck(err, "clCreateKernel", "create kernel");
    
    return kernel;
}