//
//  TerminalPrint.c
//  OpenCL
//
//  Created by Alberto Ottimo on 21/12/14.
//
//

#include "TerminalPrint.h"

int getCols()
{
    struct winsize window;
    ioctl(0, TIOCGWINSZ, &window);
    return window.ws_col > 0 ? window.ws_col : 0;
}


void printMarks(const char mark, size_t numberOfMarks)
{
    for (int i = 0; i < numberOfMarks; ++i) {
        printf("%c", mark);
    }
}

void printCenterString(const char *string, const char mark)
{
    struct winsize window;
    ioctl(0, TIOCGWINSZ, &window);
    
    size_t lenString = strlen(string);
    size_t cols = getCols();
    cols = cols < lenString ? lenString+2 : cols;
    
    size_t nMarks = (cols - lenString - 2) / 2;
    
    printMarks(mark, nMarks);
    printf(" %s ", string);
    printMarks(mark, nMarks);
    printf("\n");
}

void printEndLine(const char mark)
{
    printMarks(mark, getCols());
    printf("\n\n");
}