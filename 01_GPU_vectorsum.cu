//
//  01_GPU_vectorsum.cpp
//  
//
//  Created by Alberto Ottimo on 24/10/14.
//
//

#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime_api.h>

void check_cuda(cudaError_t err, const char *msg)
{
    if (err != cudaSuccess) {
        fprintf(stderr, "%s -- %d: %s\n", msg, err, cudaGetErrorString(err));
        exit(1);
    }
}

__global__
void init(int *v1, int *v2, int numels)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;

    if (i >= numels) return;

    v1[i] = i;
    v2[i] = numels - i;
}

__global__
void sum(const int *v1, const int *v2, int *vsum, int numels)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;

    if (i >= numels) return;

    vsum[i] = v1[i] + v2[i];
}

#define BLOCK_SIZE 1024

cudaEvent_t evt[2];

void do_init(int *v1, int *v2, int numels)
{
    int threadsPerBlock = BLOCK_SIZE;
    int numBlocks = (numels + BLOCK_SIZE -1) / threadsPerBlock;

    cudaEventRecord(evt[0]);
    init<<<numBlocks, threadsPerBlock>>>(v1, v2, numels);
    cudaEventRecord(evt[1]);

    cudaError_t error = cudaEventSynchronize(evt[1]);
    check_cuda(error, "init sync");

    float ms;
    error = cudaEventElapsedTime(&ms, evt[0], evt[1]);
    printf("Inizializzazione: %f\n", ms);
}

void do_sum(const int *v1, const int *v2, int *vsum, int numels)
{
    int threadsPerBlock = BLOCK_SIZE;
    int numBlocks = (numels + BLOCK_SIZE -1) / threadsPerBlock;

    cudaEventRecord(evt[0]);
    sum<<<numBlocks, threadsPerBlock>>>(v1, v2, vsum, numels);
    cudaEventRecord(evt[1]);

    //cudaError_t error = cudaThreadSynchronize();
    cudaError_t error = cudaEventSynchronize(evt[1]);
    check_cuda(error, "sum sync");

    float ms;
    error = cudaEventElapsedTime(&ms, evt[0], evt[1]);
    printf("Somma: %f\n", ms);
}

int main (int argc, char * argv[])
{
    if (argc != 2) {
        fprintf(stderr, "Specificare numero argomenti");
        exit(1);
    }


    /* Getting info from GPU[0] (alla prima gpu del computer in uso)
    cudaDeviceProp props;
    cudaError_t error = cudaGetDeviceProperties(&props, 0);
    printf("gridMax: %dx%dx%d\n", props.maxGridSize[0], props.maxGridSize[1], props.maxGridSize[2]);
     */

    cudaError_t err;
    int numels = atoi(argv[1]);

    int *dv1 = NULL;
    int *dv2 = NULL;
    int *dvsum = NULL;

    size_t memsize = numels * sizeof(*dv1);
    printf("MemSize: %ld\n", memsize);

    err = cudaMalloc(&dv1, memsize);
    check_cuda(err, "cudaMalloc v1");
    err = cudaMemset(dv1, 1, memsize);
    check_cuda(err, "cudaMemset v1");

    err = cudaMalloc(&dv2, memsize);
    check_cuda(err, "cudaMalloc v2");
    err = cudaMemset(dv2, 2, memsize);
    check_cuda(err, "cudaMemset v2");

    err = cudaMalloc(&dvsum, memsize);
    check_cuda(err, "cudaMalloc vsum");
    err = cudaMemset(dvsum, 3, memsize);
    check_cuda(err, "cudaMemset vsum");

    int *hvsum = (int *)malloc(memsize);


    //CUDA Event
    err = cudaEventCreate(&evt[0]);
    check_cuda(err, "event one");
    err = cudaEventCreate(&evt[1]);
    check_cuda(err, "event two");

    do_init(dv1, dv2, numels);
    do_sum(dv1, dv2, dvsum, numels);


    cudaEventRecord(evt[0]);
    err = cudaMemcpy(hvsum, dvsum, memsize, cudaMemcpyDeviceToHost);
    cudaEventRecord(evt[1]);
    check_cuda(err, "cudaMemcpy");

    cudaEventSynchronize(evt[1]);
    printf("CudaCopy: %f\n", ms);

    cudaEventElapsedTime(&ms, evt[0], evt[1]);

    printf("Controllo...\n");
    for (int i = 0; i < numels; ++i) {
        if (hvsum[i] != numels) {
            fprintf(stderr, "Controllo fallito");
            exit(1);
        }
    }

    cudaFree(dv1);
    cudaFree(dv2);
    cudaFree(dvsum);
    free(hvsum);

    return 0;
}