//
//  0_vector_sum.cpp
//  
//
//  Created by Alberto Ottimo on 22/10/14.
//
//

#include <stdio.h>
#include <stdlib.h>

#include "Library/ClockTime.h"

void do_init(int *v1, int *v2, int numels)
{
    for (int i = 0; i < numels; ++i) {
        v1[i] = i;
        v2[i] = numels - i;
    }
}

void do_sum(int *v1, int *v2, int *vsum, int numels)
{
    for (int i = 0; i < numels; ++i) {
        vsum[i] = v1[i] + v2[i];
    }
}

int main (int argc, char * argv[])
{
    if (argc != 2) {
        fprintf(stderr, "Specificare numero argomenti");
        exit(1);
    }

    int numels = atoi(argv[1]);

    int *v1 = NULL;
    int *v2 = NULL;
    int *vsum = NULL;

    size_t memsize = numels * sizeof(*v1);

    v1 = (int *)malloc(memsize);
    v2 = (int *)malloc(memsize);
    vsum = (int *)malloc(memsize);

    if (v1 == NULL || v2 == NULL || vsum == NULL) {
        free(v1);
        free(v2);
        free(vsum);
        fprintf(stderr, "Errore nell'allocazione di memoria\n");
        exit(1);
    }

    //timespec temps[2];

    double start, end;

    start = getTimeMilliSeconds();
    printf("Inizializzazione: ");
    do_init(v1, v2, numels);
    end = getTimeMilliSeconds();
    printf("%fms\n", end - start);

    start = getTimeMilliSeconds();
    printf("Somma: ");
    do_sum(v1, v2, vsum, numels);
    end = getTimeMilliSeconds();
    printf("%fms\n", end - start);


    start = getTimeMilliSeconds();
    printf("Controllo: ");
    for (int i = 0; i < numels; ++i) {
        if (vsum[i] != numels) {
            fprintf(stderr, "Controllo fallito\n");
        }
    }
    end = getTimeMilliSeconds();
    printf("%fms\n", end - start);


    free(v1);
    free(v2);
    free(vsum);
}